<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kursus</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">

</head>

<body>
    <!-- Navbar -->
    <nav class="navbar fixed-top" style="background-color: #00D8D6;">
        <a class="navbar-brand" href="#"
            style="font-family: 'Rubik', sans-serif; font-size: 30px; font-weight: 900; color: white">BelajarYuk!</a>
        <button class="navbar-toggler" type="button" data-toggler="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle Navigation">
            <span class="navbar-toggler-icon"></span>
        </button>


        <ul class="nav justify-content-end" style="font-family: 'Rubik', sans-serif; font-size: 22px;">
            <li class="nav-item">
                <a class="nav-link" href="Halaman-home.php" style="color: white">Home</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="Menu Kursus.php" style="color: white; border-bottom: 2px solid white">Kursus
                    <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Menu Programs.php" style="color: white">Program</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Menu Teacher.php" style="color: white">Pengajar</a>
            </li>
        </ul>

    </nav>
    <!-- Batas Navbar -->
    <br>
    <br>
    <br>
    <br>
    <div class="text-center" style="font-family: 'Rubik', sans-serif; font-weight: 700; color: #34E7E4">
        <h1 style="font-size: 60px"><b>KURSUS</b></h1>
    </div>
    <br>
    <!-- Bagian Menu Kursus -->
    <div class="container">
        <div class="card-deck">
            <?php
            include ('config/connection.php');
            $query = mysqli_query($conn,"SELECT kursus.id, kursus.nama_kursus, kursus.id_mentor, kursus.harga, mentor.nama_lengkap_mentor as 'Nama Mentor' FROM kursus INNER JOIN mentor ON kursus.id_mentor=mentor.id;");
            
            while ($fetch = mysqli_fetch_array($query)) {
               
               ?>
               <div class="card" style="width: 250px">
                <img class="card-img-top" src="https://this.deakin.edu.au/wp-content/uploads/2016/05/Pink-desk-with-computer.jpg" alt="img" width="250px">
                <div class="card-body">
                    <h4 class="card-title"><?php echo $fetch['nama_kursus']; ?></h4>
                    <p>Kursus <?php echo $fetch['nama_kursus']; ?> ? Disini aja!</p>
                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal<?php echo $fetch['id'];?>">
                    Rp.<?php echo $fetch['harga'];?>.000/bln</a>
                </div>
            </div>
            <!-- Modal Kursus-->
        <div class="modal fade" id="modal<?php echo $fetch['id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Kursus <?php echo $fetch['nama_kursus'];?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <!-- Detail Kursus-->

                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                        <div class="card" style="width:250px">
                                                <img class="card-img-top"
                                                    src="img/<?php echo $fetch['Nama Mentor'];?>.png"
                                                    alt="Card image" style="width: 80px; margin: auto;">
                                                <div class="card-body text-center">
                                                    <h4 class="card-title"><?php echo $fetch['Nama Mentor']; ?></h4>
                                                    <p class="card-text">Pengajar <?php echo $fetch['nama_kursus'];?></p>
                                                    <p class="card-text">Pengajar yang senang hati membuat pelajar senang dalam mata pelajaran yang sedang diajarkan</p>
                                                </div>
                                            </div>
                                </div>
                                <div class="col text-center">
                                    <p style="padding-top: 100px;"><b>Rp. <?php echo $fetch['harga'];?>.000</b>
                                        <br>
                                        30 Hari
                                    </p>
                                    <a href="daftarkursus.php?id=<?php echo $fetch['id'];?>&kursus=<?php echo $fetch['nama_kursus'];?>&harga=<?php echo $fetch['harga'];?>&mentor=<?php echo $fetch['Nama Mentor']; ?>&idMentor=<?php echo $fetch['id'];?>" class="btn btn-success" style="width: 100%;" name="submit">Berlangganan Sekarang!</a>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
               <?php
            }
            ?>
            
        </div>
        <!-- Script CSS -->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.18.6/dist/sweetalert2.all.min.js"></script>
        <script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
</body>

</html>