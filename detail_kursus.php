<?php
session_start();
if (!isset($_SESSION['email'])) {
  echo "<script>alert('Please Login First');window.location.href='Halaman-home.php'</script>";
}
include('config/connection.php');
$idKursus = $_GET['idKursus'];
$user = "SELECT * FROM users where id=" . $_SESSION['id'];
$kursus = "SELECT * FROM kursus where id=" . $idKursus;

$materiquery = "SELECT kursus.nama_kursus, judul_materi, materi.id FROM materi INNER JOIN kursus ON materi.id_kursus = kursus.id WHERE kursus.id=" . $idKursus;
$query = mysqli_query($conn, $materiquery);
$seeU = mysqli_query($conn, $user);
$seeK = mysqli_query($conn, $kursus);

$u = mysqli_fetch_array($seeU);
$k = mysqli_fetch_array($seeK);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title><?= $u['nama_panggilan']; ?>'s List Kursus</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
  <style>
    body {
      font-family: "Lato", sans-serif;
    }

    .sidebar {
      height: 100%;
      width: 200px;
      position: fixed;
      z-index: 1;
      top: 0;
      left: 0;
      background-color: #24d4d4;
      overflow-x: hidden;
      padding-top: 16px;
      margin-top: 4%;


    }

    .sidebar a {
      padding: 6px 8px 6px 16px;
      text-decoration: none;
      font-size: 20px;
      color: floralwhite;
      display: block;
    }

    .sidebar a:hover {
      color: cadetblue;
    }

    .sidebar a:active {
      color: cadetblue;
    }

    .main {
      margin-left: 160px;
      /* Same as the width of the sidenav */
      padding: 0px 10px;
      margin-top: 4%;
    }

    @media screen and (max-height: 450px) {
      .sidebar {
        padding-top: 15px;
      }

      .sidebar a {
        font-size: 18px;
      }
    }

    .nav-link :hover {
      color: white;
    }
  </style>
</head>

<body>

  <nav class="navbar fixed-top" style="background-color: #00D8D6;">
    <a class="navbar-brand" href="Halaman-home.php" style="font-family: 'Rubik', sans-serif; font-size: 30px; font-weight: 900; color: white">BelajarYuk!</a>
    <button class="navbar-toggler" type="button" data-toggler="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle Navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="d-flex flex-row-reverse bd-highlight">

      <div class="nav-item dropdown" style="padding: 10px;" style="font-family: 'Rubik', sans-serif; font-size: 22px;">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-family: 'Rubik', sans-serif; font-size: 22px; color:white;">
          Hai, <?= $u['nama_panggilan']; ?>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="edit_profile_user.php">Edit Profile</a>
          <a class="dropdown-item" href="logout.php">Logout</a>
        </div>

      </div>
    </div>

  </nav>

  <br>
  <div class="sidebar">

    <a href="dashboard-home.php"><i class="fa fa-fw fa-home"></i> Home</a>
    <a href="daftar_kursus_user.php"><i class="fa fa-fw fa-wrench"></i> <strong>Daftar Kursus</strong> </a>


  </div>

  <!-- <ul>
        <li><a href="">Beranda</a></li>
        <li> <a href="">Kursus Saya</a></li>
    </ul> -->
  </div>
  <div class="main" style="margin-left: 15%;">
    <h1 style="text-align: center; font-size:50px;"><i>Selamat Datang, <?= $u['nama_panggilan']; ?></i></h1>
    <br>
    <h2><strong>Materi <?php echo $k['nama_kursus']; ?> :</strong></h2>
    <br>
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">No</th>
          <th scope="col">Nama Materi</th>

        </tr>
      </thead>
      <tbody>

        <?php

        $nomor = 1;
      while ($d = mysqli_fetch_array($query)) {
        # code...
      

        ?>

        <tr>
          <td><?php echo $nomor++; ?></td>
          <td><a href="detail_materi.php?idMateri=<?= $d['id'] ?>"><?php echo $d['judul_materi']; ?></a></td>
        </tr>
      </tbody>
      <?php
      }
      ?>
    </table>
  </div>

  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.18.6/dist/sweetalert2.all.min.js"></script>
  <script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>