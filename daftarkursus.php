<?php
session_start();
$kursus = $_GET['kursus'];
$id = $_GET['id'];
$mentor = $_GET['mentor'];
$harga = $_GET['harga'];
$idMentor = $_GET['idMentor'];
// if (isset($_SESSION['email'])) {
# code...


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Daftarin Kursus..</title>
    <link rel="stylesheet" href="https://unpkg.com/browse/sweetalert@1.1.3/dist/sweetalert.css">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.18.6/dist/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
</head>

<body>
    <!-- Navbar -->
    <nav class="navbar fixed-top" style="background-color: #00D8D6;">
        <a class="navbar-brand" href="#" style="font-family: 'Rubik', sans-serif; font-size: 30px; font-weight: 900; color: white">BelajarYuk!</a>
        <button class="navbar-toggler" type="button" data-toggler="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle Navigation">
            <span class="navbar-toggler-icon"></span>
        </button>


        <ul class="nav justify-content-end" style="font-family: 'Rubik', sans-serif; font-size: 22px;">
            <li class="nav-item">
                <a class="nav-link" href="Halaman-home.php" style="color: white">Home</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="Menu Kursus.php" style="color: white; border-bottom: 2px solid white">Kursus
                    <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Menu Programs.php" style="color: white">Program</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Menu Teacher.php" style="color: white">Pengajar</a>
            </li>
        </ul>

    </nav>
    <br>
    <br>
    <br>
    <br>
    <div class="container">
        <div class="row">
            <div class="col text-center">
                <h1><b>Rincian Pembayaran</b></h1>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col text-center">
                <h1>Kelas yang dipilih</h1>
                <hr>
            </div>
            <div class="col text-center">
                <h1><b><?= $kursus ?></b></h1>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col text-center">
                <h1>Biaya</h1>
                <hr>
            </div>
            <div class="col text-center">
                <h1><b>Rp.<?= $harga ?>.000,-</b></h1>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col text-center">
                <h1>Durasi</h1>
                <hr>
            </div>
            <div class="col text-center">
                <h1><b>30 Hari</b></h1>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col text-center">
                <h1>Metode Pembayaran</h1>
                <hr>
            </div>
            <div class="col text-center">
                <select class="form-control form-control-lg">
                    <option class="text-center">Jenius</option>
                    <option>Mandiri</option>
                </select>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col text-center">
                <a href="buy.php?id=<?=$id?>&idMentor=<?=$idMentor?>"><button class="btn btn-success" style="width: 250px; height: 50px; font-size: 18pt;"><b>BAYAR</b></button></a>
                
            </div>
        </div>
    </div>
</body>

</html>


<?php
// } else {
//     echo "        
//             <script type='text/javascript'>
//             swal.fire({type: 'error', title: 'Oops!',text: 'Anda belum login!'}).then(function(){
//             window.location = 'home.php';
//                 }
//             );
//         </script>";
// }
?>